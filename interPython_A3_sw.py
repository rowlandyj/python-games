# template for "Stopwatch: The Game"
import simplegui

# define global variables
running = False
t = 0
hits = 0
tries = 0

# added this for fun, see how many hits you get in a row
combo_num = 0

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    if t < 10:
        return "0:00." + str(t)
    s = str(t)
    tenths = s[-1]
    minutes = int(s[:-1]) / 60
    seconds = int(s[:-1]) - (minutes * 60)
    minutes = str(minutes)
    if seconds < 10:
        seconds = "0" + str(seconds)
    else:
        seconds = str(seconds)
    out = minutes + ":" + seconds + "." + tenths
    return out
    
# define event handlers for buttons; "Start", "Stop", "Reset"
def reset():
    global t, hits, tries, running, combo_num
    t = 0
    hits = 0
    tries = 0
    combo_num = 0
    running = False
    

def start():
    global running
    if not running:
        running = True
        timer.start()
        
def stop():
    global running, hits, tries, combo_num
    if running:
        running = False
        timer.stop()
        tries += 1
        if t % 10 == 0:
            hits += 1
            combo_num += 1
        else:
            combo_num = 0
            

# define event handler for timer with 0.1 sec interval
def tick():
    global t, running
    t += 1
    running = True

# define draw handler
def draw(canvas):
    canvas.draw_text(format(t), [40, 180], 90, 'Red')
    canvas.draw_text(str(hits) + "/" + str(tries), [180, 70], 60, 'Green')
    canvas.draw_text("Combo: " + str(combo_num), [40, 280], 60, 'Blue')
    
# create frame & timer
frame = simplegui.create_frame('StopWatch', 300, 300)
timer = simplegui.create_timer(100, tick)
button1 = frame.add_button('Start', start, 100)
button2 = frame.add_button('Stop', stop, 100)
button3 = frame.add_button('Reset', reset, 100)

# register event handlers
frame.set_draw_handler(draw)


# start frame
frame.start()


# Please remember to review the grading rubric
