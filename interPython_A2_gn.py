# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console
import simplegui
import random


# initialize global variables used in your code
game_range = 100
correct_num = random.randrange(0, game_range)
max_guesses = 7
num_guesses = 0


# helper function to start and restart the game
def new_game():
    global correct_num, num_guesses
    correct_num = random.randrange(0, game_range)
    num_guesses = 0
    print "New Game Started With Range of 0 to " + str(game_range)
    print "Number of remaining guesses is " + str(max_guesses)
    print
    
# To print the top line. Could have also just used print statements 
new_game()

# define event handlers for control panel
def range100():
    # button that changes range to range [0,100) and restarts
    global game_range, max_guesses
    game_range = 100
    max_guesses = 7
    new_game()

def range1000():
    # button that changes range to range [0,1000) and restarts
    global game_range, max_guesses
    game_range = 1000
    max_guesses = 10
    new_game()
    
def input_guess(guess):
    # main game logic goes here 
    global num_guesses
    num_guesses += 1
    guesses_left = max_guesses - num_guesses
    
    print "Guess was " + str(guess)
    print "Number of remaining guesses is " + str(guesses_left)
    
    if num_guesses == max_guesses and int(guess) != correct_num:
        print "You ran out of guesses. The number was " + str(correct_num)
        print
        new_game()
    elif int(guess) < correct_num:
        print "Higher!"
        print
    elif int(guess) > correct_num:
        print "Lower!"
        print
    else:
        print "Correct!"
        print "New Game Starting Now!"
        print
        new_game()
    
    
    
# create frame
frame = simplegui.create_frame("Guess the Number", 300, 300)

# Got bored so I added some text to the canvas, not needed for proj
def draw(canvas):
    canvas.draw_text("Range of: 0 - " + str(game_range), 
                                        [80, 120], 20, 'Blue')
    canvas.draw_text("Number of Guesses Left: " + str(max_guesses - num_guesses),
                     [40, 140], 20, 'Red')

# register event handlers for control elements
frame.add_button("Restart", new_game)
frame.add_button("Range: 0 - 100", range100, 200)
frame.add_button("Range: 0 - 1000", range1000, 200)
frame.add_input("Input your guess", input_guess, 100)

# handler for the canvas text, not needed for proj
frame.set_draw_handler(draw)


# call new_game and start frame
frame.start()


# always remember to check your completed program against the grading rubric
