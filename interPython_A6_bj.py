# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")    

# initialize some useful global variables
in_play = False
outcome = ""
options = "Hit or Stand?"
score = 0

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        self.hand = []
        self.is_turn = False

    def __str__(self):
        # return a string representation of a hand
        s = "Hand contains "
        for card in self.hand:
            s += str(card) + " "
        return s

    def add_card(self, card):
        self.hand.append(card)

    def turn(self):
        self.is_turn = True
    
    def get_value(self):
        # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        value = 0
        aces = 0
        for card in self.hand:
            card_rank = card.get_rank()
            if card_rank == "A":
                aces += 1 
            value += VALUES[card.get_rank()]
        if aces == 0:
            return value
        else:
            if value + 10 <= 21:
                return value + 10
            else:
                return value
   
    def draw(self, canvas, pos):
        shift = 0
        if not self.is_turn:
            canvas.draw_image(card_back, (CARD_BACK_CENTER[0], CARD_BACK_CENTER[1]), CARD_BACK_SIZE, [pos[0] + CARD_BACK_CENTER[0], pos[1] + CARD_BACK_CENTER[1]], CARD_BACK_SIZE)
            self.hand[1].draw(canvas, [pos[0] + 100, pos[1]])
        else:
            for card in self.hand:
                card.draw(canvas, [pos[0] + shift, pos[1]])
                shift += 100
 
        
# define deck class 
class Deck:
    def __init__(self):
        self.deck = []
        for suit in SUITS:
            for rank in RANKS:
                self.deck.append(Card(suit, rank))

    def shuffle(self):
        # shuffle the deck 
        random.shuffle(self.deck)

    def deal_card(self):
        # deal a card object from the deck
        return self.deck.pop()
    
    def __str__(self):
        s = "Deck contains "
        for card in self.deck:
            s += str(card) + " "
        return s

player_hand = Hand()
dealer_hand = Hand()
deck = Deck()


#define event handlers for buttons
def deal():
    global outcome, in_play, deck, player_hand, dealer_hand, score, options
    outcome = ""
    
    if in_play:
        score -= 1
        outcome = "Last Hand Folded"
    
    options = "Hit or Stand?"
    deck = Deck()
    player_hand = Hand()
    dealer_hand = Hand()
    deck.shuffle()
    player_hand.add_card(deck.deal_card())
    dealer_hand.add_card(deck.deal_card())
    player_hand.add_card(deck.deal_card())
    dealer_hand.add_card(deck.deal_card())
    player_hand.turn()
    
    in_play = True

def hit():
    global outcome, score, player_hand, in_play, options, dealer_hand
    # if the hand is in play, hit the player, score
    if in_play:
        player_hand.add_card(deck.deal_card())
        if player_hand.get_value() > 21:
            outcome = "You Have Busted"
            options = "Deal Again?"
            dealer_hand.turn()
            score -= 1
            in_play = False
    
    # if busted, assign a message to outcome, update in_play and score
       
def stand():
    global score, dealer_hand, outcome, in_play, options
   
    if not in_play:
        return
    
    dealer_hand.turn()
    
    # if hand is in play, repeatedly hit dealer until his hand has value 17 or more
    while in_play:
        if dealer_hand.get_value() < 17:
            dealer_hand.add_card(deck.deal_card())
        else:
            in_play = False
    
    if dealer_hand.get_value() >= player_hand.get_value() and dealer_hand.get_value() <= 21:
        outcome = "You Lose!"
        score -= 1
    elif player_hand.get_value() > dealer_hand.get_value():
        outcome = "You Win!"
        score += 1
    elif dealer_hand.get_value() > 21:
        outcome = "Dealer Busted. You Win!"
        score += 1
    options = "Deal again?"
    
    # assign a message to outcome, update in_play and score

# draw handler    
def draw(canvas):
    global score
    # test to make sure that card.draw works, replace with your code below
    canvas.draw_text("Blackjack", (50,100), 50, "Red")
    canvas.draw_text("Score: " + str(score), (375, 100), 50, "Black")
    canvas.draw_text("Dealer", (100, 200), 30, "Black")
    canvas.draw_text("Player", (100, 400), 30, "Black")
    canvas.draw_text(outcome, (275, 200), 30, "Black")
    canvas.draw_text(options, (275, 400), 30, "Black")
    card = Card("S", "A")
    player_hand.draw(canvas, [100, 425])
    dealer_hand.draw(canvas, [100, 225])


# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
deal()
frame.start()


# remember to review the gradic rubric