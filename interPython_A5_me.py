# implementation of card game - Memory

import simplegui
import random

cards = range(8) + range(8)
exposed = list([False]) * len(cards)
turns = 0
state = 0
index = None
# I used a list here instead of another int var for the matching/unmatching logic 
unpaired = []



# helper function to initialize globals
def new_game():
    global cards, turns, state, exposed, index
    turns = 0
    state = 0
    random.shuffle(cards)
    index = None
    unpaired = []
    exposed = list([False]) * len(cards)

     
# define event handlers
def mouseclick(pos):
    global index, state, exposed, turns, unpaired
    old_index = index
    index = pos[0] // 50
    
    # making sure nothing happens if you click on an already flipped card
    if not exposed[index]:
        if state == 0:
            state = 1
        
        if state == 1:
            if len(unpaired) != 0:
                exposed[unpaired[0]] = False
                exposed[unpaired[1]] = False
                unpaired = []
            state = 2
            exposed[index] = True
        # turn count will increase when second card is chosen
        else:
            turns += 1
            state = 1
            exposed[index] = True
            if cards[index] != cards[old_index]:
                unpaired = [index, old_index]
        
    
                        
# cards are logically 50x100 pixels in size    
def draw(canvas):
    global cards
    
    # Use for loop and built-in enumerate function to get
    # bool value, ex, and the index, i, of the value
    label.set_text("Turns = " + str(turns))
    for i, ex in enumerate(exposed):
        if ex:
            canvas.draw_text(str(cards[i]), [12 + 50 * i, 65], 50, "Blue")
        else:
            canvas.draw_polygon([(i * 50, 0), (i * 50, 100), 
                         (50 + i * 50, 100), (50 + i * 50, 0)], 
                         2, "Black", "Green")
        
    


# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = " + str(turns))


# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()


# Always remember to review the grading rubric